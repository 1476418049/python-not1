import allure

from python.Huameichaoshi.api.user_api import send_code
from python.Huameichaoshi.utils.read import base_data
@allure.feature("用户中心模块")  #allure测试报告的模块
class TestUser:   #class类
    @allure.story("用户注册后登录")
    @allure.title("注册手机号测试用例")  #allure测试报告的标签
    def test_register(self):
        json_data = base_data.read_data()['test_register']
        send_code(json_data)