import logging
# 配置日志的地方
import os.path

# 先寻找到文件  上级文件
import time

root_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
print(root_path)
# 把log文件 与root_path结合
log_path = os.path.join(root_path, "log")


# 定义一个log的类
class Logger:
    def __init__(self):
        # 定义日志位置和文件名                                                 #定义当前时间
        self.logname = os.path.join(log_path, "{}.log".format(time.struct_time("%Y-%m-%d")))
        # 定义一个日志容器
        self.logger = logging.getLogger("log")
        # 设置日志打印的级别
        self.logger.setLevel(logging.DEBUG)
        # 创建日志输入的格式
        self.formater = logging.Formatter(
            '[%(asctime)s][%(filename)s %(lineno)d][%(levelname)s]: %(message)s')
        # 创建日志处理器，在控制台打印
        self.console = logging.StreamHandler()
        # 设置控制台打印日志界别
        self.console.setLevel(logging.DEBUG)
        # 文件存放日志级别
        self.filelogger.setLevel(logging.DEBUG)
        # 文件存放日志格式
        self.filelogger.setFormatter(self.formater)
        # 控制台打印日志格式
        self.console.setFormatter(self.formater)
        # 将日志输出渠道添加到日志收集器中
        self.logger.addHandler(self.filelogger)
        self.logger.addHandler(self.console)


# 先测试一下
logger = Logger().logger

if __name__ == '__main__':
    logger.debug("我打印debug日志")
    logger.info("我打印info日志")
    logger.warning("我打印warning日志")
    logger.error("我打印erro日志")
