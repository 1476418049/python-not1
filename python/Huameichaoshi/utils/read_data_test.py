import yaml
import os

#1.打开f这个路径下的文件
#f = open("../data/data.yaml", encoding="utf8")#相对路径
# #2.读取f这个文件    把值赋值给data
# data = yaml.safe_load(f)
# print(data)
# print(data["test"])

#使用parametrize结合yaml文件
#使用这个命令来找到yamk文件的路径然后吧 这个path带入到下面的f = （。。。。）这里面
path = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))),"data","data.yaml")
#1.
def read_yaml():
    f = open(path, encoding="utf8")  # 相对路径
    data = yaml.safe_load(f)
    return  data

get_data  = read_yaml()