import yaml


# #1.打开f这个路径下的文件
# f = open("../data/data.yaml",encoding="utf8")#相对路径
# #2.读取f这个文件    把值赋值给data
# data = yaml.safe_load(f)

def read_data():
    # 1.打开f这个路径下的文件
    f = open("../data/data.yaml", encoding="utf8")  # 相对路径
    # 2.读取f这个文件    把值赋值给data
    data = yaml.safe_load(f)
    return data  # 结束掉data


# 把read_data 赋值给get  data这个
if __name__ == '__main__':
    print(read_data()['heros_nama'])
    names = read_data()['heros_nama']
    print(type(names))
    name_data = []
    for i in list(names):
        name_data.append((i,))
    print(name_data)

