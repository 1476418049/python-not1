import yaml
import os
import  configparser  #引入这个
#1.打开f这个路径下的文件
#f = open("../data/data.yaml", encoding="utf8")#相对路径
# #2.读取f这个文件    把值赋值给data
# data = yaml.safe_load(f)
# print(data)
# print(data["test"])


#使用parametrize结合yaml文件
#使用这个命令来找到yamk文件的路径然后吧 这个path带入到下面的f = （。。。。）这里面
#读取yaml文件 文件地址
data_path = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))),"data","data.yaml")
#获取settings.ini 文件地址
ini_path = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))),"config","settings.ini")

#1.
class FileRead:
    def __init__(self):
        self.data_path = data_path
        self.ini_path = ini_path


    def read_yaml(self):
        f = open(data_path, encoding="utf8")  # 相对路径
        data = yaml.safe_load(f)
        return  data

    def read_ini(self):
        config = configparser.ConfigParser()  # 实例化的对象
        config.read(ini_path, encoding='utf-8')  # 设置格式
        return config

#read() 方法用于从文件读取指定的字节数，如果未给定或为负则读取所有。
base_data = FileRead()
#get_data  = read_yaml()
#get_ini = read_ini()

#尝试读取测试一下，有返回数据就说明可以成功
#print(read_ini()['host']['api_sit_url'])