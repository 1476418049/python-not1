import  configparser  #引入这个
import os

#获取settings.ini 文件地址
path = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))),"config","settings.ini")

def read_ini():
    config = configparser.ConfigParser()   #实例化的对象
    config.read(path,encoding='utf-8')    #设置格式
    return config

get_ini = read_ini()

#尝试读取测试一下，有返回数据就说明可以成功
print(read_ini()['host']['api_sit_url'])