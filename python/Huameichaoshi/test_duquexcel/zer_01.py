import openpyxl

# 打开EXcel文件
workceshi = openpyxl.load_workbook('../test_duquexcel/验证用例.xls')

# 获取第一个工作表
worksheel = workceshi.active

# 遍历第一个工作表的每一行，并打印出每一行的值
for row in worksheel.iter_rows():
    for call in row:
        print(call.value, end='')
    print()
