import  pytest
#使用mark来把参数带入到下面
# @pytest.mark.parametrize("name,word",[["安琪拉","火烧云"]])
# def test_parametrize_03(name,word):
#     print(f'{name}的台词是{word}')
from Huameichaoshi.utils.read_data import read_data
#  比如 你的参数 是 id  value
# 数据要组装好成后面  列表内 每条数据都放在元组内，这样 pytest才能识别~！
# 你想只传name 那么你就要找到yaml里面每个下面的name 组成元组
# 此外 用yaml的话，了解下yaml在这里面是怎么写的

get_data = read_data()
names = get_data['heros_nama']
print(type(names))
name_data = []  # 你传入几个参数，就要有相应的对应的参数 ("id, value, sex",[(1,"xx", "m"),(2,"yy", "f"),(1,"xx", "m")])
# pytest 第一次执行 是将  id=1 value="xx" sex="m"  第二次是 id=2 value="yy" sex="f"  你列表内有几个元组 他就循环几次
#for i 是一个遍历循环
for i in list(names):
    name_data.append((i,))
print(name_data)

#这个是多循环模式
@pytest.mark.parametrize("id, value, sex",[(1,"xx", "m"),(2,"yy", "f"),(3,"xx", "c")])
def test_parametrize_03(id, value, sex):
    print(f"id={id},value={value},sex={sex}")  # f=format~!? python3.7以上版本的字符串格式化
    # 传统的如下
    print("id={},value={},sex={}".format(id, value, sex))


# #单次循环模式测试
# @pytest.mark.parametrize("a",["liu","han","wei"])  #这里误区之一@pytest.mark.parametrize("a，b",["liu","han"]) ，
# # 后面多加了一个b，那么下面就要同步def加上b，不然就不是单循环了，要一一对应，
# def test_parametrize_04(a):
#     print(f"我是={a}")




#参数值为字典
@pytest.mark.parametrize("hero",[{"name":"刘欢"},{"name":"李龙"},{"name":"约翰"}])
def test_paiamet_05(hero):
    print(hero['name'])




if __name__ == '__main__':
    pytest.main(["-vs"])

